<?php

namespace backend\controllers;

use backend\services\SystemMenuService;
use Yii;
use yii\data\Pagination;
use backend\models\SystemMenu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\LinkPager;
/**
 * AdminModuleController implements the CRUD actions for SystemMenu model.
 */
class AdminModuleController extends BaseController
{
	public $layout = "lte_main";

    /**
     * Lists all SystemMenu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new SystemMenu();
        list($querys,$models, $pages, $totalCount, $startNum, $endNum) = $this->getList();
        if (Yii::$app->request->isAjax == true) {
            $result = ['models'=>$models, 'pages'=>$pages, 'totalNum'=>$totalCount,
            'startNum'=>$startNum,'endNum'=>$endNum];
            return $this->asJson(['errno'=>0, 'data'=>$result]);
        }
        else{
            return $this->render('index', [
            'query'=>json_encode($querys),
            'modelLabel'=>json_encode($model->attributeLabels()),
            'models'=>json_encode($models), 'pages'=>$pages, 'totalNum'=>$totalCount,
            'startNum'=>$startNum,
            'endNum'=>$endNum
            ]);
        }
    }

    private function getList(){
        $query = SystemMenu::find();
        $querys = Yii::$app->request->get('query');
        $querys['type']=SystemMenuService::$TYPE_MODULE;
        if(empty($querys)== false && count($querys) > 0){
            $condition = "";
            $parame = array();
            foreach($querys as $key=>$value){
                $value = trim($value);
                if(empty($value) == false){
                    $parame[":{$key}"]=$value;
                    if(empty($condition) == true){
                        $condition = " {$key}=:{$key} ";
                    }
                    else{
                        $condition = $condition . " AND {$key}=:{$key} ";
                    }
                }
            }
            if(count($parame) > 0){
                $query = $query->where($condition, $parame);
            }
        }
        $totalCount = $query->count();
        $pagination = new Pagination([
            'totalCount' =>$totalCount,
            'pageSize' => '10',
            'pageParam'=>'page',
            'pageSizeParam'=>'per-page']
        );
        $pages = LinkPager::widget([
            'pagination' => $pagination,
            'nextPageLabel' => '»',
            'prevPageLabel' => '«',
            'firstPageLabel' => '首页',
            'lastPageLabel' => '尾页',
        ]);
        $pages = str_replace("\n", '', $pages);
        $orderby = Yii::$app->request->get('orderby', '');
        if(empty($orderby) == false){
            $query = $query->orderBy($orderby);
        }
        $models = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->asArray()
            ->all();
        $startNum = $pagination->getPage() * $pagination->getPageSize() + 1;
        $endNum = ($pagination->getPage() + 1) * $pagination->getPageSize();
        $endNum = $endNum < $totalCount ?  $endNum : $totalCount;
        return [$querys,$models, $pages, $totalCount,$startNum,$endNum];
    }


    /**
     * Displays a single SystemMenu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $data = $model->getAttributes();
        
        
        return $this->asJson($data);

    }

    /**
     * Creates a new SystemMenu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SystemMenu();
        if ($model->load(Yii::$app->request->post())) {
        
            $model->create_user = Yii::$app->user->identity->uname;
            $model->create_date = date('Y-m-d H:i:s');
            $model->update_user = Yii::$app->user->identity->uname;
            $model->update_date = date('Y-m-d H:i:s');        
            if($model->validate() == true && $model->save()){
                $msg = array('errno'=>0, 'msg'=>'保存成功');
                return $this->asJson($msg);
            }
            else{
                $msg = array('errno'=>2, 'data'=>$model->getErrors());
                return $this->asJson($msg);
            }
        } else {
            $msg = array('errno'=>2, 'msg'=>'数据出错');
            return $this->asJson($msg);
        }
    }

    /**
     * Updates an existing SystemMenu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @param string $name
     * @return mixed
     */
    public function actionUpdate()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
        
              $model->update_user = Yii::$app->user->identity->uname;
              $model->update_date = date('Y-m-d H:i:s');            if($model->validate() == true && $model->save()){
                $msg = array('errno'=>0, 'msg'=>'保存成功');
                return $this->asJson($msg);
            }
            else{
                $msg = array('errno'=>2, 'data'=>$model->getErrors());
                return $this->asJson($msg);
            }
        } else {
            $msg = array('errno'=>2, 'msg'=>'数据出错');
            return $this->asJson($msg);
        }
    
    }

    /**
     * Deletes an existing SystemMenu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @param string $name
     * @return mixed
     */
    public function actionDelete(array $ids)
    {
        if(count($ids) > 0){
            $c = SystemMenu::deleteAll(['in', 'id', $ids]);
            return $this->asJson(array('errno'=>0, 'data'=>$c, 'msg'=>json_encode($ids)));
        }
        else{
            return $this->asJson(array('errno'=>2, 'msg'=>''));
        }
    }

	
	 

    /**
     * Finds the SystemMenu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @param string $name
     * @return SystemMenu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SystemMenu::findOne(['id' => $id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
